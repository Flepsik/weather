package uk.co.ribot.androidboilerplate.data.remote;



import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;
import uk.co.ribot.androidboilerplate.data.model.weather.Weather;
import uk.co.ribot.androidboilerplate.data.model.weather.WeatherData;
import uk.co.ribot.androidboilerplate.util.MyGsonTypeAdapterFactory;

public interface WeatherService {
    @GET("weather?")
    Observable<WeatherData> getWeather(@Query("q") String city, @Query("appid") String appId);

    class Creator {
        public static final String BASE_URL = "http://api.openweathermap.org/data/2.5/";

        public static WeatherService newWeatherService() {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .build();
            return retrofit.create(WeatherService.class);
        }
    }
}
