package uk.co.ribot.androidboilerplate.ui.sign_in;

import uk.co.ribot.androidboilerplate.ui.base.Presenter;

public interface SignInPresenter extends Presenter<SignInMvpView> {
    void signIn(String login, String password);
}
