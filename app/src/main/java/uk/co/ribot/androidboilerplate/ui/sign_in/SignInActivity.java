package uk.co.ribot.androidboilerplate.ui.sign_in;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.ribot.androidboilerplate.R;
import uk.co.ribot.androidboilerplate.data.model.weather.WeatherData;
import uk.co.ribot.androidboilerplate.ui.base.BaseActivity;

public class SignInActivity extends BaseActivity implements SignInMvpView {
    @Inject
    DefaultSignInPresenter presenter;

    @BindView(R.id.main_container)
    View mainContainer;
    @BindView(R.id.login_input)
    EditText loginInput;
    @BindView(R.id.password_input)
    EditText passwordInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        setContentView(R.layout.activity_sign_in);
        ButterKnife.bind(this);
        presenter.attachView(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @Override
    public void showError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showError(@StringRes int errorId) {
        Toast.makeText(this, errorId, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showWeather(WeatherData weatherData) {
        Snackbar.make(mainContainer, getString(
                R.string.weather_message,
                weatherData.getName(),
                String.valueOf((int) (weatherData.getMain().getTemp() - 273.15))),
                Snackbar.LENGTH_LONG)
                .show();
    }

    @OnClick(R.id.enter_button)
    public void onLoginClicked(View view) {
        presenter.signIn(loginInput.getText().toString(), passwordInput.getText().toString());
    }

    public static void start(Activity activity) {
        Intent intent = new Intent(activity, SignInActivity.class);
        activity.startActivity(intent);
    }
}
