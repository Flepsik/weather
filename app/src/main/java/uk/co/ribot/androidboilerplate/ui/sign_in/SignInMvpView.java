package uk.co.ribot.androidboilerplate.ui.sign_in;

import android.support.annotation.StringRes;

import uk.co.ribot.androidboilerplate.data.model.weather.WeatherData;
import uk.co.ribot.androidboilerplate.ui.base.MvpView;

public interface SignInMvpView extends MvpView {
    void showError(String error);

    void showError(@StringRes int stringId);

    void showWeather(WeatherData weatherData);
}
