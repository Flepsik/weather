package uk.co.ribot.androidboilerplate.ui.sign_in;

import java.util.List;

import javax.inject.Inject;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;
import uk.co.ribot.androidboilerplate.R;
import uk.co.ribot.androidboilerplate.data.DataManager;
import uk.co.ribot.androidboilerplate.data.model.Ribot;
import uk.co.ribot.androidboilerplate.data.model.weather.WeatherData;
import uk.co.ribot.androidboilerplate.ui.base.BasePresenter;
import uk.co.ribot.androidboilerplate.ui.base.Presenter;
import uk.co.ribot.androidboilerplate.util.RxUtil;

public class DefaultSignInPresenter extends BasePresenter<SignInMvpView> implements SignInPresenter {
    private final DataManager dataManager;
    private Subscription mSubscription;
    private PasswordValidator passwordValidator = new PasswordValidator();
    private EmailValidator emailValidator = new EmailValidator();

    @Inject
    public DefaultSignInPresenter(final DataManager dataManager) {
        this.dataManager = dataManager;
    }

    @Override
    public void detachView() {
        super.detachView();
        if (mSubscription != null) mSubscription.unsubscribe();
    }


    @Override
    public void signIn(String login, String password) {
        checkViewAttached();
        if (!validateLogin(login)) {
            getMvpView().showError(R.string.login_error);
            return;
        }

        if (!validatePassword(password)) {
            getMvpView().showError(R.string.password_error);
            return;
        }

        RxUtil.unsubscribe(mSubscription);
        mSubscription = dataManager.getWeather()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<WeatherData>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e, "There was an error loading the weather.");
                        getMvpView().showError(R.string.loading_error);
                    }

                    @Override
                    public void onNext(WeatherData weatherData) {
                        if (weatherData != null) {
                            getMvpView().showWeather(weatherData);
                        }
                    }
                });
    }

    private boolean validateLogin(String login) {
        return emailValidator.validate(login);
    }

    private boolean validatePassword(String password) {
        return passwordValidator.validate(password);
    }
}
