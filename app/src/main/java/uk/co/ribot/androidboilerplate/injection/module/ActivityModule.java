package uk.co.ribot.androidboilerplate.injection.module;

import android.app.Activity;
import android.content.Context;

import dagger.Module;
import dagger.Provides;
import uk.co.ribot.androidboilerplate.data.DataManager;
import uk.co.ribot.androidboilerplate.injection.ActivityContext;
import uk.co.ribot.androidboilerplate.ui.sign_in.DefaultSignInPresenter;
import uk.co.ribot.androidboilerplate.ui.sign_in.SignInPresenter;

@Module
public class ActivityModule {

    private Activity mActivity;

    public ActivityModule(Activity activity) {
        mActivity = activity;
    }

    @Provides
    Activity provideActivity() {
        return mActivity;
    }

    @Provides
    @ActivityContext
    Context providesContext() {
        return mActivity;
    }

    @Provides
    @ActivityContext
    SignInPresenter provideSignInPresenter(DataManager dataManager) {
        return new DefaultSignInPresenter(dataManager);
    }
}
